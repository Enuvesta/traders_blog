$(document).ready(function() {
	$('#rate-up').click(function(){
	var postid;
	var plus = 1;
	postid = $(this).attr("data-postid");
	 $.get('/post/' + postid + '/rate_post/', {'post_id': postid, 'plus': plus}, function(data){
	 		   rating = 'Рейтинг: ' + data.rating;
	           $('.rates').html(rating);
	           if (data.message != ''){
	           		$('.alr_voted').css('visibility', 'visible');
	           }
	       });
	});
	$('#rate-down').click(function(){
	var postid;
	var plus = -1;
	postid = $(this).attr("data-postid");
	 $.get('/post/' + postid + '/rate_post/', {'post_id': postid, 'plus': plus}, function(data){
	 		   rating = 'Рейтинг: ' + data.rating;
	           $('.rates').html(rating);
	           if (data.message != ''){
	           		$('.alr_voted').css('visibility', 'visible');
	           }
	       });
	});
	$('#rate-up-user').click(function(){
	var userid;
	var plus = 1;
	userid = $(this).attr("data-userid");
	 $.get('/user/' + userid + '/rate_user/', {'user_id': userid, 'plus': plus}, function(data){
	           $('#cur').html(data.rating);
	           if (data.message != ''){
	           		$('.alr_voted_user').css('visibility', 'visible');
	           }
	       });
	});
	$('#rate-down-user').click(function(){
	var userid;
	var plus = -1;
	userid = $(this).attr("data-userid");
	 $.get('/user/' + userid + '/rate_user/', {'user_id': userid, 'plus': plus}, function(data){
	 		   $('#cur').html(data.rating);
	           if (data.message != ''){
	           		$('.alr_voted_user').css('visibility', 'visible');
	           }
	       });
	});
	$('*[data-confirm="true"]').click(function() {
    	return confirm("Are you sure?");
	});
	// $('.log-in_button').click(function(){
	// 	$.get('/', {}, function(data){
	//  		    if (data.message == ''){
	// 				alert("worked!");
	// 	       		$('.error').css('visibility', 'visible');
	// 	       	}
	//        });
 //    });
});