from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from .models import Post, CustomUser, Comment, Post_voter, User_voter
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from .forms import CommentForm
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from .forms import PostForm, RegistrationForm, UpdateProfile
from tagging.models import Tag
from django.db.models import Count, Q
from functools import reduce
import operator
import json


def post_list(request):
    posts_list = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)

    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def traders(request):
    authors = CustomUser.objects.filter(trader=True)
    posts_list = Post.objects.filter(author__in=authors).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def companies(request):
    authors = CustomUser.objects.filter(company=True)
    posts_list = Post.objects.filter(author__in=authors).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def banks(request):
    authors = CustomUser.objects.filter(bank=True)
    posts_list = Post.objects.filter(author__in=authors).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def brokers(request):
    authors = CustomUser.objects.filter(broker=True)
    posts_list = Post.objects.filter(author__in=authors).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    form = CommentForm()
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post.html', {'post': post, 'form': form, 'news': news, 'tops': tops})

def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return HttpResponseRedirect('')
    else:
        form = CommentForm()
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post.html', {'form': form, 'post': post, 'news': news, 'tops': tops})

def user_detail(request, pk):
    user = get_object_or_404(CustomUser, pk=pk)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/user.html', {'user': user, 'news': news, 'tops': tops})

@login_required
def rate_post(request, pk):
    context = RequestContext(request)
    post_id = None
    if request.method == 'GET':
        post_id = request.GET['post_id']
        plus = request.GET['plus']

    rating = 0
    if post_id:
        post = Post.objects.get(pk=int(post_id))
        if Post_voter.objects.filter(post_id=post_id, user_id=request.user.id).exists():
            rating = post.rating
            jsonDict = { "rating": rating, "message": "everything's fine" }
            return HttpResponse( json.dumps(jsonDict), content_type="application/json")
        else:
            pv = Post_voter(user=request.user, post=post)
            pv.save()
            rating = post.rating + int(plus)
            post.rating =  rating
            post.save()
    jsonDict = { "rating": rating, "message": ''}
    return HttpResponse( json.dumps(jsonDict), content_type="application/json")
@login_required
def rate_user(request, pk):
    context = RequestContext(request)
    user_id = None
    if request.method == 'GET':
        user_id = request.GET['user_id']
        plus = request.GET['plus']

    rating = 0
    if user_id:
        user = CustomUser.objects.get(pk=int(user_id))
        if User_voter.objects.filter(user_1_id=request.user.id, user_2_id=user_id).exists():
            rating = user.rating
            jsonDict = { "rating": rating, "message": "everything's fine"}
            return HttpResponse( json.dumps(jsonDict), content_type="application/json")
        else:
            uv = User_voter(user_1=request.user, user_2=user)
            uv.save()
            rating = user.rating + int(plus)
            user.rating =  rating
            user.save()
    jsonDict = { "rating": rating, "message": ''}
    return HttpResponse( json.dumps(jsonDict), content_type="application/json")
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_edit.html', {'form': form, 'news': news, 'tops': tops})
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('blog.views.post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_edit.html', {'form': form, 'news': news, 'tops': tops})

def post_delete(request,pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return HttpResponseRedirect('/')

def add_to_fave(request,pk):
    user = request.user
    post = get_object_or_404(Post, pk=pk)
    if user.favorites.filter(pk=pk).exists():
        user.favorites.remove(post)
    else:
        user.favorites.add(post)
    return HttpResponseRedirect('/post/'+pk+'/')

def favorite_posts(request):
    user = request.user
    posts_list = user.favorites.all().order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})


def myposts(request):
    posts_list = Post.objects.filter(author=request.user).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def mycomments(request):
    comm = Comment.objects.filter(author=request.user)
    ids = []
    for c in comm:
        ids.append(c.post.pk)
    posts_list = Post.objects.filter(pk__in=ids).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def tagged(request, pk):
    tag = get_object_or_404(Tag, pk=pk)
    posts_list = Post.objects.all().filter(tags__iregex=r"[[:<:]]%s[[:>:]]" % tag).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def register_user(request):
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'registration/register_success.html', {'news': news, 'tops': tops})
    else:
        form = RegistrationForm()
    return render(request, 'registration/register.html', {'form': form, 'news': news, 'tops': tops})

def user_posts(request, pk):
    user = CustomUser.objects.get(pk=pk)
    posts_list = Post.objects.filter(author=user).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def user_comments(request, pk):
    user = CustomUser.objects.get(pk=pk)
    comm = Comment.objects.filter(author=user)
    ids = []
    for c in comm:
        ids.append(c.post.pk)
    posts_list = Post.objects.filter(pk__in=ids).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})
def user_favorites(request, pk):
    user = CustomUser.objects.get(pk=pk)
    posts_list = user.favorites.all().order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})
def search(request):
    query = request.GET.get('q')
    query_list = query.split()
    if query:
        posts_list = Post.objects.filter(
                reduce(operator.and_,
                       (Q(title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(text__icontains=q) for q in query_list))  |
                reduce(operator.and_,
                       (Q(tags__icontains=q) for q in query_list))  
            ).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})
def subscribe(request, pk):
    you = request.user
    user = get_object_or_404(CustomUser, pk=pk)
    print(user)
    if you.subscriptions.filter(pk=pk).exists():
        you.subscriptions.remove(user)
    else:
        you.subscriptions.add(user)
        print(you.subscriptions.all)
    return HttpResponseRedirect('/user/'+pk+'/')
def subcriptions_list(request, pk):
    user = CustomUser.objects.get(pk=pk)
    authors = user.subscriptions.all
    posts_list = Post.objects.filter(author__in=authors).order_by('-published_date')
    paginator = Paginator(posts_list, 5)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        posts = paginator.page(paginator.num_pages)
    news = Post.objects.order_by('-rating')[:6]
    tops = Post.objects.annotate(num_comments=Count('comments')).order_by('-num_comments')[:6]
    return render(request, 'blog/post_list.html', {'posts': posts, 'news': news, 'tops': tops})

def comment_delete(request,pk, pkc):
    comment = get_object_or_404(Comment, pk=pkc)
    comment.delete()
    return HttpResponseRedirect('/post/'+pk+'/')

def update_profile(request):
    args = {}

    if request.method == 'POST':
        form = UpdateProfile(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('')
    else:
        form = UpdateProfile()

    args['form'] = form
    return render(request, 'registration/update_profile.html', args)

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        # Правильный пароль и пользователь "активен"
        auth.login(request, user)
        # Перенаправление на "правильную" страницу
        return HttpResponseRedirect("/account/loggedin/")
    else:
        # Отображение страницы с ошибкой
        return render(request, 'registration/login.html')