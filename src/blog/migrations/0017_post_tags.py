# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tagging_autocomplete.models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0016_remove_post_summary'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='tags',
            field=tagging_autocomplete.models.TagAutocompleteField(blank=True, max_length=255),
        ),
    ]
