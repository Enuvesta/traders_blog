# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0022_auto_20160508_1751'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscribers',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
            ],
        ),
        migrations.AddField(
            model_name='customuser',
            name='subscriptions',
            field=models.ManyToManyField(to='blog.CustomUser'),
        ),
        migrations.AddField(
            model_name='subscribers',
            name='creator',
            field=models.ForeignKey(related_name='follower', to='blog.CustomUser'),
        ),
        migrations.AddField(
            model_name='subscribers',
            name='friend',
            field=models.ForeignKey(related_name='followed_by', to='blog.CustomUser'),
        ),
    ]
