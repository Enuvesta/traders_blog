# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0038_auto_20160529_0135'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='votes',
            field=models.ForeignKey(null=True, to='blog.CustomUser', related_name='voted_by'),
        ),
    ]
