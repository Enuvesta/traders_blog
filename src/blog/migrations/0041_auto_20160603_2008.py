# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0040_auto_20160603_1836'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post_voter',
            name='post',
        ),
        migrations.RemoveField(
            model_name='post_voter',
            name='user',
        ),
        migrations.AddField(
            model_name='post_voter',
            name='user_1',
            field=models.ForeignKey(to='blog.CustomUser', null=True, related_name='who'),
        ),
        migrations.AddField(
            model_name='post_voter',
            name='user_2',
            field=models.ForeignKey(to='blog.CustomUser', null=True, related_name='whom'),
        ),
    ]
