# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_auto_20160502_1613'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='rating',
            field=models.BigIntegerField(default=0),
        ),
    ]
