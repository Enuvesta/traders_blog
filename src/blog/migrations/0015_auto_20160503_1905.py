# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_auto_20160503_0243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='rating',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='post',
            name='text',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
