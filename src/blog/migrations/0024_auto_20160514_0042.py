# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0023_auto_20160514_0029'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='favorite',
            name='post',
        ),
        migrations.RemoveField(
            model_name='favorite',
            name='user',
        ),
        migrations.RemoveField(
            model_name='subscribers',
            name='creator',
        ),
        migrations.RemoveField(
            model_name='subscribers',
            name='friend',
        ),
        migrations.AlterField(
            model_name='customuser',
            name='subscriptions',
            field=models.ManyToManyField(related_name='followed_by', to='blog.CustomUser'),
        ),
        migrations.DeleteModel(
            name='Favorite',
        ),
        migrations.DeleteModel(
            name='Subscribers',
        ),
    ]
