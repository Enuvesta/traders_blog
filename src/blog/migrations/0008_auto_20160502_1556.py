# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_customuser'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='contacts',
            field=models.CharField(max_length=100, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='location',
            field=models.CharField(max_length=100, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='private',
            field=models.CharField(max_length=200, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='rating',
            field=models.BigIntegerField(default=0),
            preserve_default=False,
        ),
    ]
