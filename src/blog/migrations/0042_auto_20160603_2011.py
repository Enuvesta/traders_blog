# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0041_auto_20160603_2008'),
    ]

    operations = [
        migrations.CreateModel(
            name='User_voter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_1', models.ForeignKey(related_name='who', null=True, to='blog.CustomUser')),
                ('user_2', models.ForeignKey(related_name='whom', null=True, to='blog.CustomUser')),
            ],
        ),
        migrations.RemoveField(
            model_name='post_voter',
            name='user_1',
        ),
        migrations.RemoveField(
            model_name='post_voter',
            name='user_2',
        ),
        migrations.AddField(
            model_name='post_voter',
            name='post',
            field=models.ForeignKey(to='blog.Post', null=True),
        ),
        migrations.AddField(
            model_name='post_voter',
            name='user',
            field=models.ForeignKey(to='blog.CustomUser', null=True),
        ),
    ]
