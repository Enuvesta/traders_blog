from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, UserManager
from tagging_autocomplete.models import TagAutocompleteField
from tagging.models import Tag

from ckeditor.fields import RichTextField


class Post(models.Model):
    title = models.CharField(max_length=200)
    published_date = models.DateTimeField(default=timezone.now)
    text = RichTextField()
    tags = TagAutocompleteField()
    rating = models.BigIntegerField(default=0)
    author = models.ForeignKey('blog.CustomUser', null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
    def get_tags(self):
        return Tag.objects.get_for_object(self)

class Comment(models.Model):
    date = models.DateTimeField(default=timezone.now)
    text = models.TextField()
    post = models.ForeignKey('blog.Post', related_name='comments')
    author = models.ForeignKey('blog.CustomUser', null=True)
    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

class CustomUser(User):
    """User with app settings."""
    date_of_birth = models.DateField(blank=True, null=True)
    private = models.CharField(max_length=200)
    rating = models.BigIntegerField(default=0)
    contacts = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    trader = models.BooleanField(default=False)
    company = models.BooleanField(default=False)
    bank = models.BooleanField(default=False)
    broker = models.BooleanField(default=False)
    favorites = models.ManyToManyField(Post, related_name='favorited_by')
    subscriptions = models.ManyToManyField('blog.CustomUser', related_name='followed_by')
    avatar = models.ImageField(upload_to='uploads/', default = 'uploads/ava.jpg')
    # Use UserManager to get the create_user method, etc.
    objects = UserManager()

class Post_voter(models.Model):
    user = models.ForeignKey(CustomUser, null=True)
    post = models.ForeignKey(Post, null=True)

class User_voter(models.Model):
    user_1 = models.ForeignKey(CustomUser, related_name='who', null=True)
    user_2 = models.ForeignKey(CustomUser, related_name='whom', null=True)

# class Favorite(models.Model):
#     user = models.ForeignKey(CustomUser, unique=False)
#     post = models.ForeignKey(Post, unique=False)

# class Subscribers(models.Model):
#     creator = models.ForeignKey(CustomUser, unique=False, related_name='follower')
#     friend = models.ForeignKey(CustomUser, unique=False, related_name='followed_by')

def __unicode__(self):
    return self.user.username