from django import forms
from captcha.fields import CaptchaField

from .models import Post, Comment, CustomUser
from django.core.files.images import get_image_dimensions

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'text', 'tags')

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)

class RegistrationForm(forms.ModelForm):
    captcha = CaptchaField()
    password1 = forms.CharField(
        label="Password",
        widget=forms.PasswordInput,
    )
    password2 = forms.CharField(
        label="Password confirmation",
        widget=forms.PasswordInput,
        help_text="Enter the same password as before, for verification.",
    )
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'password1', 'password2',
                  'date_of_birth', 'private', 'contacts',
                  'location', 'trader', 'company', 'bank',
                  'broker',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return self.cleaned_data


    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class ImageUploadForm(forms.Form):
    """Image upload form."""
    image = forms.ImageField()

class UpdateProfile(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ('avatar', 'username', 'email', 'first_name', 'last_name')

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']

        try:
            w, h = get_image_dimensions(avatar)

            #validate dimensions
            max_width = max_height = 200
            print(w,h)
            if w > max_width or h > max_height:
                raise forms.ValidationError(
                    u'Please use an image that is '
                     '%s x %s pixels or smaller.' % (max_width, max_height))

            #validate content type
            # main, sub = avatar.content_type.split('/')
            # if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
            #     raise forms.ValidationError(u'Please use a JPEG, '
            #         'GIF or PNG image.')

            #validate file size
            if len(avatar) > (100 * 1024):
                raise forms.ValidationError(
                    u'Avatar file size may not exceed 20k.')

        except TypeError:
            """
            Handles case when we are updating the user profile
            and do not supply a new avatar
            """
            pass

        return avatar
    def clean_email(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if email and CustomUser.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError('This email address is already in use. Please supply a different email address.')
        return email

    def save(self, commit=True):
        user = super(UpdateProfile, self).save(commit=False)
        user.email = self.cleaned_data['email']
        self.fields['avatar'].required = False

        if commit:
            user.save()

        return user
