from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^post/(?P<pk>\d+)/comment/$', views.add_comment_to_post, name='add_comment_to_post'),
    url(r'^user/(?P<pk>[0-9]+)/$', views.user_detail, name='user_detail'),
    url(r'^post/(?P<pk>\d+)/rate_post/$', views.rate_post, name='rate_post'),
    url(r'^user/(?P<pk>\d+)/rate_user/$', views.rate_user, name='rate_user'),
    url(r'^traders/$', views.traders, name='traders'),
    url(r'^companies/$', views.companies, name='companies'),
    url(r'^banks/$', views.banks, name='banks'),
    url(r'^brokers/$', views.brokers, name='brokers'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^myposts/$', views.myposts, name='myposts'),
    url(r'^mycomments/$', views.mycomments, name='mycomments'),
    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
    url(r'^post/(?P<pk>[0-9]+)/delete/$', views.post_delete, name='post_delete'),
    url(r'^post/(?P<pk>[0-9]+)/add_to_fave/$', views.add_to_fave, name='add_to_fave'),
    url(r'^tagged/(?P<pk>[0-9]+)/$', views.tagged, name='tagged'),
    url(r'^register_user/$', views.register_user, name='register_user'),
    url(r'^favorite_posts/$', views.favorite_posts, name='favorite_posts'),
    url(r'^user_posts/(?P<pk>[0-9]+)/$', views.user_posts, name='user_posts'),
    url(r'^user_comments/(?P<pk>[0-9]+)/$', views.user_comments, name='user_comments'),
    url(r'^user_favorites/(?P<pk>[0-9]+)/$', views.user_favorites, name='user_favorites'),
    #url(r'^user_subscriptions/(?P<pk>[0-9]+)/$', views.user_posts, name='user_posts'),
    url(r'^search/*$', views.search, name='search'),
    url(r'^user/(?P<pk>[0-9]+)/subscribe/$', views.subscribe, name='subscribe'),
    url(r'^subcriptions_list/(?P<pk>[0-9]+)$', views.subcriptions_list, name='subcriptions_list'),
    url(r'^post/(?P<pk>[0-9]+)/comment_delete/(?P<pkc>[0-9]+)$', views.comment_delete, name='comment_delete'),
    url(r'^update_profile/$', views.update_profile, name='update_profile'),
    url(r'^/login/$', views.login, name='login')
]

