Spelling and grammar - done
Check website in all browsers - done
Decide on www-subdomain - done
MobileOK score of 75+ - done
HTML5 compatibility check - done
Custom 404 page - almost done
Favicon - done
Use friendly URLs - done
robots.txt - done
HTML validation - almost done
CSS validation done
Run CSS Lint - done
Color contrast - done
Add humans.txt - done